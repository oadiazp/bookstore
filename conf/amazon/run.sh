#!/bin/bash

APP=bookstore
USER=ubuntu

cd /home/$USER/apps/$APP

source venv/bin/activate
exec uwsgi -c src/uwsgi.ini
