# -*- coding: utf-8 -*-

from __future__ import with_statement
from fabric.api import env, require, run, sudo, cd, prefix
from fabric.contrib.files import exists

#--------------------------------------
#environments
#--------------------------------------


def amazon():
    "Servidor local de prueba"
    env.name = 'amazon'
    env.user = 'ubuntu'
    env.project_name = 'bookstore'
    env.project_root = '/home/%s/apps/%s/src' % (env.user, env.project_name)
    env.project_path = '/home/%s/apps/%s/' % (env.user, env.project_name)
    env.hosts = ['bookstore.zczoft.com']
    env.use_ssh_config = False
    env.key_filename = '/home/zcool/.ssh/id_rsa.pub'
    env.branch = 'master'
    env.repo = 'http://oadiazp@bitbucket.org/oadiazp/bookstore.git'
    env.venv = '/home/%(user)s/apps/%(project_name)s/venv' % env


def common():
    with cd(env.project_root):
        #actualizar los settings de la app
        sudo("cp -Rf conf/%s/local_settings.py %s" % (env.name, env.project_root))

        #sincronizar los modelos
        run('./manage.py syncdb')

        #correr las migraciones
        run('./manage.py migrate')

        #recojer los estáticos
        run('./manage.py collectstatic')

        # #actualizar los settings de nginx
        sudo("cp -Rf conf/%s/nginx /etc/nginx/sites-available/%s.conf" % (env.name, env.project_name))
        sudo("ln -fs /etc/nginx/sites-available/%s.conf /etc/nginx/sites-enabled/%s.conf" % (env.project_name, env.project_name))

        #actualizar los settings de gunicorn
        sudo("cp -Rf conf/%s/uwsgi.ini %s" % (env.name, env.project_root))
        sudo("cp -Rf conf/%s/uwsgi_params %s" % (env.name, env.project_root))
        sudo("cp -Rf conf/%s/run.sh %s" % (env.name, env.project_root))

        #actualizar la conf de supervisor
        sudo("cp -Rf conf/%s/supervisor.conf /etc/supervisor/conf.d/%s.conf" % (env.name, env.project_name))

        #reiniciar el servicio
        sudo("service nginx restart")
        sudo("service supervisor restart")
        sudo('supervisorctl restart %s' % env.project_name)


def update():
    require('name')
    require('project_root')

    with cd(env.project_root):
        with prefix("source %(venv)s/bin/activate" % env):
            #darle un pull al servidor
            run('git pull')

            dependencies()
            common()

def dependencies():
        with cd(env.project_root):
            #Instalando dependencias desde pip
            sudo("pip install -r conf/%s/requirements.txt" % env.name)


def cloning_repo():
    with cd(env.project_path):
        run('git clone %s src/' % (env.repo))


def create_directories():
    run('mkdir -p %s' % env.project_path)


def create_log_directory():
    run('mkdir -p %s/log' % env.project_path)


def creating_virtualenv():
    run('virtualenv %s' % env.venv)

def install():
    sudo("apt-get install python-pip git-core supervisor nginx gcc")
    sudo("apt-get install python-virtualenv virtualenvwrapper")

    create_directories()
    cloning_repo()
    create_log_directory()
    creating_virtualenv()

    with prefix("source %(venv)s/bin/activate" % env):
        run('easy_install -U distribute')

        dependencies()
        create_log_directory()
        common()
