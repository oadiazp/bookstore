Installation steps:
________________________________________________________________________________

1. Clone the project from https://bitbucket.org/oadiazp/bookstore.git.
2. Create a virtualenv
3. Activate the virtualenv
4. Go to the working copy directory.
5. Install the dependencies: pip install -r requirements.pip.
6. Create the database: python manage.py syncdb
7. Run the migrations: python manage.py migrate
8. Collect the static files: python manage.py collectstatic
9. Run the development server: python manage.py runserver
10. Access to http://localhost:8000.

Feel free to ask me any question you have.

The can be accesed by: http://bookstore.zczoft.com.
