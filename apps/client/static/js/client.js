angular.module('Bookstore', [
    'ui.bootstrap'
]);

angular.module('Bookstore').
controller('BookCtrl', function($scope, $http, $modal){
    $scope.load = function(){
         var url = 'api/books/';

        if ($scope.page)
            url += '?page=' + $scope.page;

        $http.get(url)
             .success(function(response){
                $scope.next = response.next;
                $scope.prev = response.previous;
                $scope.books = response.results;
             }
        );
    }

    $scope.delete = function(book){
        $http.delete('api/books/' + book.id)
             .success(function(){
                $scope.load();
             });
    }

    var me = $scope;

    $scope.add = function(){
        var modal = $modal.open({
            templateUrl: 'frmBook',
            size: 'md',
            controller: function($scope, $modalInstance){
                $scope.save = function (book) {
                    $http.post('api/books/', book).success(function(){
                        me.load();
                        $modalInstance.close();
                    })
                }
            }
        });
    }

    $scope.update = function(book){
        var modal = $modal.open({
            templateUrl: 'frmBook',
            size: 'md',
            controller: function($scope, $modalInstance){
                $scope.book = book;

                $scope.save = function (book) {
                    $http.put('api/books/' + book.id + '/', book).success(function(){
                        me.load();
                        $modalInstance.close();
                    })
                }
            }
        });
    }

    $scope.prevPage = function(){
        $scope.page = $scope.getParameterByName($scope.prev, 'page');
        $scope.load();
    }

    $scope.nextPage = function(){
        $scope.page = $scope.getParameterByName($scope.next, 'page');
        $scope.load();
    }

    $scope.getParameterByName = function(url, name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(url);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    $scope.load();
});