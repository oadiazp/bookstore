from django.conf.urls import patterns, url
from apps.client.views import IndexView


urlpatterns = patterns('',
                       url(r'^$',
                           IndexView.as_view(),
                           name='index'),
)
