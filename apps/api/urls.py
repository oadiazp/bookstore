from django.conf.urls import patterns, url, include
from rest_framework import viewsets, routers
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from apps.api.models import Book
from apps.api.serializers import BookSerializer


class BookViewSet(viewsets.ModelViewSet):
    """
    API Viewset

    This class provides all methods for the books (POST, PUT, GET, DELETE)
    """
    model = Book
    serializer_class = BookSerializer
    paginate_by = 5
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

#Router that creates the urls
router = routers.DefaultRouter()
router.register(r'books', BookViewSet)

#Adding routes to the Django URLs.
urlpatterns = patterns('',
    url(r'^', include(router.urls)),
)
