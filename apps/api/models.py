from django.db import models
from django.contrib.sites.models import Site

class Book(models.Model):
    """
    Entity to modelate book instances
    """

    name = models.CharField(max_length=100)
    isbn = models.CharField(max_length=100)
    price = models.FloatField()
    in_house = models.BooleanField()

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        """
        Method to generate the corresponding url to the book
        """

        site = Site.objects.get_current()
        
        return "http://%s/api/books/%s" % (site.domain, str(self.id))
