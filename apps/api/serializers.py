from rest_framework.serializers import ModelSerializer, CharField
from apps.api.models import Book


class BookSerializer(ModelSerializer):
    """
    Class to define how to serialize a book
    """

    url = CharField(source='get_absolute_url', read_only=True)

    class Meta:
        model = Book
        fields = ('id', 'name', 'isbn', 'price', 'in_house', 'url')
