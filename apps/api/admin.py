from django.contrib import admin
from apps.api.models import Book

admin.site.register(Book)